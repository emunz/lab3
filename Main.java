public class Main {

    public static void main(String[] args) {
        Player player = new HumanFlashCardPlayer("Name"); //put your name here
        FlashCardGame game = new FlashCardGame(player);


        FlashCard[] cardList = {}; //put all FlashCards you want to use in this array
        game.setCards(cardList);

        game.play();

    }
}
