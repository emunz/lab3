public class FlashCardGame extends OnePlayerGame {

    int numCards, numRounds, numCorrect;
    FlashCard[] cards;


    /* game constructor
     */
    public FlashCardGame(Player player) {
        super(player);
    }

    /* instantiates the cards[] array with an input array of card objects
        instantiates numCards with the size of cards[]
     */
    public void setCards(FlashCard[] cards) {
    }


    /* Initiates game as a loop of "rounds"
       prints front of a random card, waits for user to input a string
       either
     */
    @Override
    public void play() {
    }


    /* generates random number between 0 and numCards - 1
        returns FlashCard object from cards array
     */
    public FlashCard getRandomCard() {
        return null;
    }


    /* compares userInput to card.back,
       updates card object's correctGuesses or incorrectGuesses, &
       returns true or false
     */
    public boolean checkGuess(String userInput, FlashCard card) {
        return false;
    }

    /*  Prints out numCorrect / numRounds
     */
    public int getScore() {
        return 0;
    }

    /* For use as a study module.
        iterates through each FlashCard in cards[],
        calls getMeta() to see how many correct & incorrect guesses the user has for that card
     */
    public void getDetailedScore() {

    }

}
