##CIS 3296 Lab 3: Flash Card Game

**Team Member 1:** Elizabeth Munz	
**Team Member 2:** Tyler Simmons

###Description
The game should emulate a basic situation in which the user is being presented flash cards and may do any of the following: Take a guess what the flash card back says, get a basic score (simple number of correct and incorrect guesses), get a detailed score (iterate through all of the flash cards seeing what the cards are and seeing how many times the user guessed correctly and incorectly), or quit and close the game. The user will interact with the game through basic text input and the game will handle all parsing and handling of various user inputs.

Each "round" of the game, the user will be prompted with the front of the card (Represented by a Card object's Front String). At that point, the game will list the options the user has at that time along with how to use those options. Once the input is recieved, the game will proceed as specified by the user or reprompt due to unacceptable input. The game will loop in this manner until the user quits.

###Class Diagram
![Diagram](Diagram.jpg "Diagram")

###Code Requirements
1. The user should be able to specify the number of cards in the deck
2. The user should be able to quit the game at any time and see their detailed score for each card in the play() class (see below for example.)
3. The "cards" should be set either beforehand or read in from a file (Should happen in the main class during game setup)
4. Each card should have a "memory" e.g. how many guesses have been made at that card 
5. The front and back of the cards should be single words not containing any spaces or other extraneous phrasing (necesary dash is permitted)
	* The takeInput method should handle "cleaning" of the user input before returning it to the game logic
	
	
Pseudocode for play() class in FlashCardGame:

   

    bool donePlaying = false
    do {
     numRounds++
     getRandomCard()
     print card.getFront()
     prompt user for input
     if input = "q", donePlaying = true
     else if input = "s", getDetailedScore()
     else 
         checkGuess()
         if checkGuess() = true, numCorrect++
    } while(!donePlaying)
