public class FlashCard {
	
	private String front;
	private String back;
	private int correctGuesses;
	private int incorrectGuesses;
	
	public FlashCard(String front, String back) {
		this.front = front;
		this.back = back;
	}
	
	/*
	 * Returns the "front" of the flashcard to
	 * the calling method
	 */
	public String getFront() {
		return front;
	}
	
	/*
	 * Returns the "back" i.e. the answer
	 * to the calling method
	 */
	public String getBack() {
		return back;
	}
	
	/*
	 * (To be used by game logic) increments the internal
	 * counter of correct guesses for that instance of card
	 */
	public void addCorrect() {
		
	}
	
	/*
	 * (To be used by game logic) increments the internal
	 * counter of incorrect guesses for that instance of card
	 */
	public void addIncorrect() {
		
	}
	
	/*
	 * Returns a String that lists the front and
	 * back of the card as well as the number of
	 * times the card has been guessed correctly
	 * and incorrectly
	 */
	public String getMeta() {
		return null;
	}
	
}
