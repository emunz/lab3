public abstract class OnePlayerGame {

    Player player;

    public OnePlayerGame(Player player) {
        this.player = player;
    }


    abstract public void play();

}
